-- User Add Localisation
ALTER TABLE "users"
ADD COLUMN latitude FLOAT,
ADD COLUMN longitude FLOAT;