-- Create ENUM types
CREATE TYPE user_type AS ENUM ('USER', 'BOTANIST', 'ADMIN');
CREATE TYPE session_status AS ENUM ('ACTIVE', 'INACTIVE', 'COMPLETED');

-- USERS Table
CREATE TABLE "users"
(
    id_user      SERIAL PRIMARY KEY,
    image_user   BYTEA,
    last_name    VARCHAR(255),
    first_name   VARCHAR(255),
    email        VARCHAR(255),
    address      VARCHAR(255),
    phone_number VARCHAR(10),
    password     VARCHAR(255),
    user_type    user_type,
    note         INT
);

-- REPORTS Table
CREATE TABLE "reports"
(
    id_report         SERIAL PRIMARY KEY,
    id_reported_user  INT,
    id_reporting_user INT,
    reason            VARCHAR(255),
    details           TEXT,
    report_date       TIMESTAMP,
    CONSTRAINT fk_reported_user FOREIGN KEY (id_reported_user) REFERENCES "users" (id_user),
    CONSTRAINT fk_reporting_user FOREIGN KEY (id_reporting_user) REFERENCES "users" (id_user),
    CONSTRAINT unique_report UNIQUE (id_reported_user, id_reporting_user) -- Unique constraint to prevent multiple reports
);

-- CONVERSATIONS Table
CREATE TABLE "conversations"
(
    id_conversation   SERIAL PRIMARY KEY,
    conversation_name VARCHAR(100) NOT NULL
);

-- MESSAGES Table
CREATE TABLE "messages"
(
    id_message      SERIAL PRIMARY KEY,
    id_user         INT  NOT NULL,
    id_conversation INT  NOT NULL,
    message_text    TEXT NOT NULL,
    created_at      TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (id_user) REFERENCES "users" (id_user),
    FOREIGN KEY (id_conversation) REFERENCES "conversations" (id_conversation)
);

-- USER_CONVERSATIONS Table
CREATE TABLE "user_conversations"
(
    id_user         INT,
    id_conversation INT,
    PRIMARY KEY (id_user, id_conversation),
    FOREIGN KEY (id_user) REFERENCES "users" (id_user),
    FOREIGN KEY (id_conversation) REFERENCES "conversations" (id_conversation)
);

-- SESSIONS Table
CREATE TABLE "sessions"
(
    id_session            SERIAL PRIMARY KEY,
    start_date            TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    end_date              TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    session_status        session_status,
    latitude              FLOAT NOT NULL,
    longitude             FLOAT NOT NULL,
    id_user               INT  NOT NULL REFERENCES "users" (id_user),
    id_user_looking_after INT REFERENCES "users" (id_user)
);

-- PLANTS Table
CREATE TABLE "plants"
(
    id_plant           SERIAL PRIMARY KEY,
    plant_name         VARCHAR(255),
    plant_species      VARCHAR(255),
    watering_frequency INT,
    location           VARCHAR(255),
    plant_image        BYTEA,
    id_user            INT REFERENCES "users" (id_user)
);

-- SESSION_PLANTS Table
CREATE TABLE "session_plants"
(
    id_session INT REFERENCES "sessions" (id_session),
    id_plant   INT REFERENCES "plants" (id_plant),
    PRIMARY KEY (id_session, id_plant)
);

-- PHOTOS Table
CREATE TABLE "photos"
(
    id_photo   SERIAL PRIMARY KEY,
    date_taken TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    image      BYTEA,
    id_session INT REFERENCES "sessions" (id_session)
);

-- ADVICES Table
CREATE TABLE "advices"
(
    id_advice  SERIAL PRIMARY KEY,
    advice     TEXT,
    id_user    INT REFERENCES "users" (id_user),
    id_session INT REFERENCES "sessions" (id_session)
);
