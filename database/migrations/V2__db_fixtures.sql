-- User fixtures
INSERT INTO "users" (image_user, last_name, first_name, email, address, phone_number, password, user_type, note)
VALUES (NULL, 'Doe', 'John', 'john.doe@example.com', '123 Main St', '1234567890',
        '$2a$10$mySL2CWoK9SuB6hD7P/Xt.24ahCho7/OnMyhXEobzc.vr9Q0kICUa', 'USER', 5), --password=test
       (NULL, 'Smith', 'Jane', 'jane.smith@example.com', '456 High St', '0987654321',
        '$2a$10$mySL2CWoK9SuB6hD7P/Xt.24ahCho7/OnMyhXEobzc.vr9Q0kICUa', 'ADMIN', 10); --password=test

-- Conversation fixtures
INSERT INTO "conversations" (conversation_name)
VALUES ('Conversation 1'),
       ('Conversation 2');

-- User_Conversation fixtures
INSERT INTO "user_conversations" (id_user, id_conversation)
VALUES (1, 1),
       (2, 1),
       (2, 2);

-- Message fixtures
INSERT INTO "messages" (id_user, id_conversation, message_text)
VALUES (1, 1, 'Hello, Jane!'),
       (2, 1, 'Hello, John!');

-- Session fixtures
INSERT INTO "sessions" (session_status, latitude, longitude, id_user, id_user_looking_after)
VALUES ('ACTIVE', 48.8566, 2.3522, 1, NULL),
       ('INACTIVE', 48.8566, 2.3522, 2, NULL);

-- Plant fixtures
INSERT INTO "plants" (plant_name, plant_species, watering_frequency, location, plant_image, id_user)
VALUES ('Rose', 'Rosa', 7, 'Garden', NULL, 1),
       ('Lily', 'Lilium', 14, 'Balcony', NULL, 2);

-- Session_Plant fixtures
INSERT INTO "session_plants" (id_session, id_plant)
VALUES (1, 1),
       (2, 2);
