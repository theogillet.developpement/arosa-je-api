package com.arosaje.demo.entity;
import jakarta.persistence.*;
import org.hibernate.annotations.JdbcTypeCode;

import java.sql.Timestamp;
import java.sql.Types;

@Entity
@Table(name = "photos")
public class PhotoEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_photo")
    private Integer idPhoto;

    @Column(name = "date_taken")
    private Timestamp dateTaken;

    @Lob
    @Column(name = "image", columnDefinition = "BYTEA")
    @JdbcTypeCode(Types.VARBINARY)
    private byte[] image;

    @ManyToOne
    @JoinColumn(name = "id_session", foreignKey = @ForeignKey(name = "FK_PHOTO_SESSION"))
    private SessionEntity session;

    public PhotoEntity(Timestamp dateTaken, byte[] image, SessionEntity session) {
        this.dateTaken = dateTaken;
        this.image = image;
        this.session = session;
    }

    public PhotoEntity() {

    }

    public Integer getIdPhoto() {
        return idPhoto;
    }

    public void setIdPhoto(Integer idPhoto) {
        this.idPhoto = idPhoto;
    }

    public Timestamp getDateTaken() {
        return dateTaken;
    }

    public void setDateTaken(Timestamp dateTaken) {
        this.dateTaken = dateTaken;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public SessionEntity getSession() {
        return session;
    }

    public void setSession(SessionEntity session) {
        this.session = session;
    }
}
