package com.arosaje.demo.entity;

import com.arosaje.demo.dto.SessionStatus;
import jakarta.persistence.*;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "sessions")
public class SessionEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_session")
    private Integer idSession;

    @Column(name = "start_date")
    private Timestamp startDate;

    @Column(name = "end_date")
    private Timestamp endDate;

    @Enumerated(EnumType.STRING)
    @JdbcTypeCode(SqlTypes.NAMED_ENUM)
    @Column(name = "session_status")
    private SessionStatus status;

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    @Column(name = "latitude")
    private float latitude;

    @Column(name = "longitude")
    private float longitude;

    @ManyToOne
    @JoinColumn(name = "id_user", foreignKey = @ForeignKey(name = "FK_SESSION_USER"))
    private UserEntity user;

    public UserEntity getUserLookingAfter() {
        return userLookingAfter;
    }

    public void setUserLookingAfter(UserEntity userLookingAfter) {
        this.userLookingAfter = userLookingAfter;
    }

    @ManyToOne
    @JoinColumn(name = "id_user_looking_after", foreignKey = @ForeignKey(name = "FK_SESSION_USER_LOOKING_AFTER"))
    private UserEntity userLookingAfter;

    @ManyToMany(cascade = CascadeType.REMOVE)
    @JoinTable(
            name = "session_plants",
            joinColumns = @JoinColumn(name = "id_session"),
            inverseJoinColumns = @JoinColumn(name = "id_plant")
    )
    private List<PlantEntity> plants = new ArrayList<>();

    public Integer getIdSession() {
        return idSession;
    }

    public void setIdSession(Integer idSession) {
        this.idSession = idSession;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public Timestamp getEndDate() {
        return endDate;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    public SessionStatus getStatus() {
        return status;
    }

    public void setStatus(SessionStatus status) {
        this.status = status;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public List<PlantEntity> getPlants() {
        return plants;
    }

    public void setPlants(List<PlantEntity> plants) {
        this.plants = plants;
    }
}
