package com.arosaje.demo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "conversations")
public class ConversationEntity {

    @Id
    @Column(name = "id_conversation")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idConversation;

    @ManyToMany
    @JoinTable(
            name = "user_conversations",
            joinColumns = @JoinColumn(name = "id_conversation"),
            inverseJoinColumns = @JoinColumn(name = "id_user")
    )
    private Set<UserEntity> participants = new HashSet<>();

    @OneToMany(mappedBy = "conversation", cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<MessageEntity> messages = new HashSet<>();

    @Column(name = "conversation_name")
    private String conversationName;

    public ConversationEntity(String conversationName) {
        this.conversationName = conversationName;
    }

    public ConversationEntity() {
        this.participants = new HashSet<>();
    }

    public Long getIdConversation() {
        return idConversation;
    }

    public void setIdConversation(Long idConversation) {
        this.idConversation = idConversation;
    }

    public String getConversationName() {
        return conversationName;
    }

    public void setConversationName(String conversationName) {
        this.conversationName = conversationName;
    }

    public Set<UserEntity> getParticipants() {
        return participants;
    }

    public void setParticipants(Set<UserEntity> participants) {
        this.participants = participants;
    }

    public Set<MessageEntity> getMessages() {
        return messages;
    }

    public void setMessages(Set<MessageEntity> messages) {
        this.messages = messages;
    }

    public void addParticipant(UserEntity user) {
        participants.add(user);
    }
}
