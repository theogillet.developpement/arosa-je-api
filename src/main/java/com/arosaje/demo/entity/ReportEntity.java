package com.arosaje.demo.entity;

import jakarta.persistence.*;

import java.time.LocalDateTime;

@Entity
@Table(name = "reports")
public class ReportEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_report")
    private Long idReport;

    @ManyToOne
    @JoinColumn(name = "id_reported_user")
    private UserEntity reportedUser;

    @ManyToOne
    @JoinColumn(name = "id_reporting_user")
    private UserEntity reportingUser;

    @Column(name = "reason", length = 255)
    private String reason;

    @Column(name = "details", columnDefinition = "TEXT")
    private String details;

    @Column(name = "report_date")
    private LocalDateTime reportDate;

    public ReportEntity(Long idReport, UserEntity reportedUser, UserEntity reportingUser, String reason, String details, LocalDateTime reportDate) {
        this.idReport = idReport;
        this.reportedUser = reportedUser;
        this.reportingUser = reportingUser;
        this.reason = reason;
        this.details = details;
        this.reportDate = reportDate;
    }

    public ReportEntity() {

    }

    public Long getIdReport() {
        return idReport;
    }

    public void setIdReport(Long idReport) {
        this.idReport = idReport;
    }

    public UserEntity getReportedUser() {
        return reportedUser;
    }

    public void setReportedUser(UserEntity reportedUser) {
        this.reportedUser = reportedUser;
    }

    public UserEntity getReportingUser() {
        return reportingUser;
    }

    public void setReportingUser(UserEntity reportingUser) {
        this.reportingUser = reportingUser;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public LocalDateTime getReportDate() {
        return reportDate;
    }

    public void setReportDate(LocalDateTime reportDate) {
        this.reportDate = reportDate;
    }
}
