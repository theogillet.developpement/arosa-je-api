package com.arosaje.demo.entity;
import jakarta.persistence.*;
import org.hibernate.annotations.JdbcTypeCode;

import java.sql.Types;

@Entity
@Table(name = "plants")
public class PlantEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_plant")
    private Integer idPlant;

    @Column(name = "plant_name")
    private String namePlant;

    @Column(name = "location")
    private String location;

    @Column(name = "watering_frequency")
    private int wateringFrequency;

    @Column(name = "plant_species")
    private String species;
    @Lob
    @Column(name = "plant_image", columnDefinition = "BYTEA")
    @JdbcTypeCode(Types.VARBINARY)
    private byte[] imagePlant;

    @ManyToOne
    @JoinColumn(name = "id_user", foreignKey = @ForeignKey(name = "FK_PLANT_USER"))
    private UserEntity user;

    public PlantEntity(String namePlant, String location, int wateringFrequency, String species, byte[] imagePlant, UserEntity user) {
        this.namePlant = namePlant;
        this.location = location;
        this.wateringFrequency = wateringFrequency;
        this.species = species;
        this.imagePlant = imagePlant;
        this.user = user;
    }

    public PlantEntity() {
    }

    public int getWateringFrequency() {
        return wateringFrequency;
    }

    public void setWateringFrequency(int wateringFrequency) {
        this.wateringFrequency = wateringFrequency;
    }

    public byte[] getImagePlant() {
        return imagePlant;
    }

    public void setImagePlant(byte[] imagePlant) {
        this.imagePlant = imagePlant;
    }


    public PlantEntity(String location) {
        this.location = location;
    }

    public Integer getIdPlant() {
        return idPlant;
    }

    public void setIdPlant(Integer idPlant) {
        this.idPlant = idPlant;
    }

    public String getNamePlant() {
        return namePlant;
    }

    public void setNamePlant(String namePlant) {
        this.namePlant = namePlant;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }




    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
