package com.arosaje.demo.entity;

import jakarta.persistence.*;

@Entity
@Table(name = "advices")
public class AdviceEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_advice")
    private Integer idAdvice;

    @Column(name = "advice")
    private String adviceText;

    @ManyToOne
    @JoinColumn(name = "id_user", foreignKey = @ForeignKey(name = "FK_ADVICE_USER"))
    private UserEntity user;

    @ManyToOne
    @JoinColumn(name = "id_session", foreignKey = @ForeignKey(name = "FK_ADVICE_SESSION"))
    private SessionEntity session;

    public AdviceEntity() {
    }

    public AdviceEntity(String adviceText, UserEntity user, SessionEntity session) {
        this.adviceText = adviceText;
        this.user = user;
        this.session = session;
    }

    public String getAdviceText() {
        return adviceText;
    }

    public void setAdviceText(String adviceText) {
        this.adviceText = adviceText;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public SessionEntity getSession() {
        return session;
    }

    public void setSession(SessionEntity session) {
        this.session = session;
    }
}
