package com.arosaje.demo.controller;

import com.arosaje.demo.dto.LoginDTO;
import com.arosaje.demo.dto.UserDTO;
import com.arosaje.demo.dto.UserTokenDTO;
import com.arosaje.demo.dto.UserUpdateDTO;
import com.arosaje.demo.entity.UserEntity;
import com.arosaje.demo.security.JwtUtils;
import com.arosaje.demo.service.UserService;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private JwtUtils jwtUtils;

    @PostMapping("/login")
    public ResponseEntity<UserTokenDTO> loginUser(@RequestBody LoginDTO loginDTO) {
        UserEntity user = userService.getUserByemailandPassword(loginDTO);
        if (user != null) {
            String token = jwtUtils.generateJwtToken(user.getEmail());
            UserTokenDTO userTokenDTO = new UserTokenDTO(user, token);
            return ResponseEntity.ok(userTokenDTO);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }


    @PostMapping("/register")
    public ResponseEntity<Void> createUser(@RequestBody UserDTO userDTO) {
        userService.createUser(userDTO);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PatchMapping("/{id}")
    public ResponseEntity<Void> updateUser(@PathVariable int id, @RequestBody UserUpdateDTO userUpdateDTO) {
        boolean updated = userService.updateUser(id, userUpdateDTO);
        if (updated) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("")
    public ResponseEntity<List<UserEntity>> getAllUser() {
        List<UserEntity> utilisateurs = userService.getAllUser();
        if (!utilisateurs.isEmpty()) {
            return ResponseEntity.ok(utilisateurs);
        } else {
            return ResponseEntity.noContent().build();
        }
    }


    @GetMapping("/botanists")
    public ResponseEntity<List<UserEntity>> getBotanistUsersSortedByNote() {
        List<UserEntity> botanistUsers = userService.getBotanistUsersSortedByNote();
        return new ResponseEntity<>(botanistUsers, HttpStatus.OK);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<String> handleEntityNotFoundException(EntityNotFoundException ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User has not been found");
    }

}
