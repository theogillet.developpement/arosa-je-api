package com.arosaje.demo.controller;

import com.arosaje.demo.dto.ConversationDTO;
import com.arosaje.demo.dto.MessageDTO;
import com.arosaje.demo.dto.ParticipantDTO;
import com.arosaje.demo.entity.ConversationEntity;
import com.arosaje.demo.entity.MessageEntity;
import com.arosaje.demo.repository.MessageRepository;
import com.arosaje.demo.service.ConversationService;
import com.arosaje.demo.service.MessageService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/conversations")
public class ConversationController {

    private final MessageRepository messageRepository;

    private final ConversationService conversationService;

    public ConversationController(MessageRepository messageRepository, MessageService messageService, ConversationService conversationService) {
        this.messageRepository = messageRepository;
        this.conversationService = conversationService;
    }



    @GetMapping("{userId}")
    public List<ConversationDTO> getConversationsForUser(@PathVariable Integer userId) {
        List<ConversationEntity> conversations = conversationService.getConversationsForUser(userId);
        return conversations.stream().map(this::toConversationDTO).collect(Collectors.toList());
    }


    @GetMapping("/{conversationId}/messages")
    public List<MessageDTO> getMessagesByConversationId(@PathVariable Long conversationId) {
        List<MessageEntity> messages = messageRepository.findByConversation_IdConversation(conversationId);
        return messages.stream().map(this::toMessageDTO).collect(Collectors.toList());
    }

    private MessageDTO toMessageDTO(MessageEntity message) {
        Integer recipientId = findRecipientId(message);
        return new MessageDTO(
                message.getIdMessage(),
                message.getUser().getIdUser(),
                recipientId,
                message.getConversation().getIdConversation(),
                message.getMessageText(),
                message.getCreatedAt()
        );
    }
    private Integer findRecipientId(MessageEntity message) {
        return message.getConversation().getParticipants().stream()
                .filter(participant -> !participant.getIdUser().equals(message.getUser().getIdUser()))
                .findFirst()
                .map(participant -> participant.getIdUser())
                .orElse(null);
    }

    private ConversationDTO toConversationDTO(ConversationEntity conversation) {
        List<ParticipantDTO> participants = conversation.getParticipants().stream()
                .map(participant -> new ParticipantDTO(
                        participant.getIdUser(),
                        participant.getFirstName(),
                        participant.getLastName()))
                .collect(Collectors.toList());

        return new ConversationDTO(
                conversation.getIdConversation(),
                conversation.getConversationName(),
                participants
        );
    }



}

