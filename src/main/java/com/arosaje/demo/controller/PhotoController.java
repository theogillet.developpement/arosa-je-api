package com.arosaje.demo.controller;

import com.arosaje.demo.input.PhotoInput;
import com.arosaje.demo.service.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@RestController
@RequestMapping("/photos")
public class PhotoController {

    @Autowired
    private PhotoService photoService;

    @PostMapping("/create/{idSession}")
    public ResponseEntity<Void> createPhoto(@PathVariable Integer idSession,
                                            @RequestParam("dateTaken") String dateTakenStr,
                                            @RequestParam("image") MultipartFile image) {
        try {
            if (image.isEmpty()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
            }
            Timestamp dateTaken = Timestamp.valueOf(LocalDateTime.parse(dateTakenStr, DateTimeFormatter.ISO_DATE_TIME));

            PhotoInput photoInput = new PhotoInput(dateTaken, image.getBytes(), idSession);
            photoService.createPhoto(photoInput);
            return ResponseEntity.status(HttpStatus.CREATED).build();
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}