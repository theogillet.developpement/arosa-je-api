package com.arosaje.demo.controller;

import com.arosaje.demo.entity.PlantEntity;
import com.arosaje.demo.input.PlantInput;
import com.arosaje.demo.service.PlantService;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/plants")
public class PlantController {

    private final PlantService plantService;

    public PlantController(PlantService plantService) {
        this.plantService = plantService;
    }

    @PostMapping("/create")
    public ResponseEntity<PlantEntity> createPlant(@RequestParam("name") String nomPlante,
                                                   @RequestParam("location") String emplacement,
                                                   @RequestParam("wateringFrequency") int frequenceArrosage,
                                                   @RequestParam("species") String especePlante,
                                                   @RequestParam("idUser") int idUtilisateur,
                                                   @RequestParam("image") MultipartFile image) {
        try {
            if (image.isEmpty()) {
                System.out.println("l'image est vide");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
            }

            byte[] imageBytes = image.getBytes();

            PlantInput plantInput = new PlantInput(nomPlante, especePlante, emplacement, frequenceArrosage, imageBytes, idUtilisateur);

            PlantEntity newPlant = plantService.createPlant(plantInput, image);

            return ResponseEntity.status(HttpStatus.CREATED).body(newPlant);
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<PlantEntity> getPlantById(@PathVariable Integer id) {
        PlantEntity plante = plantService.getPlantById(id);
        return ResponseEntity.ok().body(plante);
    }


    @Transactional
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deletePlant(@PathVariable("id") Integer id) {
        boolean deleted = plantService.deletePlant(id);
        if (deleted) {
            return ResponseEntity.ok("Plant removed successfully.");
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Plant not found with id: " + id);
        }
    }

    @GetMapping("/user/{userId}")
    public ResponseEntity<List<PlantEntity>> getPlantsByUserId(@PathVariable Integer userId) {
        List<PlantEntity> plantes = plantService.getPlantsByUserId(userId);
        return new ResponseEntity<>(plantes, HttpStatus.OK);
    }

}
