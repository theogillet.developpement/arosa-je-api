package com.arosaje.demo.controller;

import com.arosaje.demo.entity.ReportEntity;
import com.arosaje.demo.entity.UserEntity;
import com.arosaje.demo.input.ReportInput;
import com.arosaje.demo.service.ReportService;
import com.arosaje.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/reports")
public class ReportController {

    @Autowired
    private ReportService reportService;

    @Autowired
    private  UserService userService;

    @PostMapping("/create")
    public ResponseEntity<String> createReport(@RequestBody ReportInput reportInput) {
        try {
            UserEntity user = userService.getUserbyId(reportInput.idUser());
            UserEntity userReporting = userService.getUserbyId(reportInput.idReportingUser());
            ReportEntity report = reportService.createReport(user, userReporting, reportInput.reason(), reportInput.details());
            return ResponseEntity.status(HttpStatus.CREATED).body("Report created successfully");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error creating report: " + e.getMessage());
        }
    }
}
