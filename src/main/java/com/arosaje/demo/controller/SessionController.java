package com.arosaje.demo.controller;

import com.arosaje.demo.dto.SessionStatus;
import com.arosaje.demo.entity.ConversationEntity;
import com.arosaje.demo.entity.SessionEntity;
import com.arosaje.demo.input.SessionInput;
import com.arosaje.demo.input.SessionPatchInput;
import com.arosaje.demo.service.ConversationService;
import com.arosaje.demo.service.SessionService;

import jakarta.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/sessions")
public class SessionController {

    private final SessionService sessionService;
    private final ConversationService conversationService;

    @Autowired
    public SessionController(SessionService sessionService, ConversationService conversationService) {
        this.sessionService = sessionService;
        this.conversationService = conversationService;
    }

    @GetMapping("/user/{userId}")
    public List<SessionEntity> getSessionsByUser(
            @PathVariable Integer userId,
            @RequestParam("status") SessionStatus status
    ) {

        return sessionService.getSessionsByUser(userId, status);
    }

    @GetMapping("/user/{userId}/uncompleted")
    public List<SessionEntity> getSessionsByUserWithCompleted(
            @PathVariable Integer userId
    ) {

        return sessionService.getSessionsByUserWithoutCompleted(userId);
    }

    @GetMapping("/user/{userId}/looking-after")
    public List<SessionEntity> getSessionsByUserLookingAfter(
            @PathVariable Integer userId
    ) {
        return sessionService.getSessionsByUserLookingAfterWithoutCompleted(userId);
    }

    @GetMapping("/user/{userId}/not-user")
    public List<SessionEntity> getSessionsNotForUser(
            @PathVariable Integer userId
    ) {
        return sessionService.getSessionsWithStatusNotUser(userId);
    }

    @PostMapping("/create")
    public ResponseEntity<SessionEntity> createSession(@RequestBody SessionInput sessionInput) {
        SessionEntity createdSession = sessionService.createSession(sessionInput);
        return new ResponseEntity<>(createdSession, HttpStatus.CREATED);
    }

    @GetMapping("/date/{userId}")
    public ResponseEntity<List<SessionEntity>> getActiveSessionsByDateRangeAndNotUser(
            @PathVariable Integer userId,
            @RequestParam("startDate") Long startDate,
            @RequestParam("endDate") Long endDate
    ) {
        Timestamp timestampStartDate = new Timestamp(startDate);
        Timestamp timestampEndDate = new Timestamp(endDate);

        List<SessionEntity> sessions = sessionService.getActiveSessionsByDateRangeAndNotUser(timestampStartDate, timestampEndDate, userId);

        return ResponseEntity.ok(sessions);
    }

    @PatchMapping("/assign")
    public ResponseEntity<Void> assignUserToSession(@RequestBody SessionPatchInput sessionInput) {
        sessionService.assignUserToSession(sessionInput.sessionId(), sessionInput.userId());

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PatchMapping("/{sessionId}/refuse-assign")
    public ResponseEntity<SessionEntity> removeUserLookingAfterAndSetActive(@PathVariable Integer sessionId) {
        try {
            SessionEntity updatedSession = sessionService.removeUserLookingAfterAndSetActive(sessionId);
            return ResponseEntity.ok(updatedSession);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

    @PatchMapping("/{sessionId}/accept-assign")
    public ResponseEntity<SessionEntity> setSessionInProgress(@PathVariable Integer sessionId) {
        try {
            SessionEntity updatedSession = sessionService.setSessionInProgress(sessionId);

                        Integer userId = updatedSession.getUser().getIdUser();
            Integer userLookingAfterId = updatedSession.getUserLookingAfter().getIdUser();

            ConversationEntity conversation = conversationService.createConversation(userId, userLookingAfterId, "Session Discussion");
            return ResponseEntity.ok(updatedSession);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }
    
}
