package com.arosaje.demo.controller;


import com.arosaje.demo.dto.EnhancedMessageDTO;
import com.arosaje.demo.dto.MessageDTO;
import com.arosaje.demo.entity.MessageEntity;
import com.arosaje.demo.service.MessageService;
import jakarta.transaction.Transactional;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class MessageController {

    private final MessageService messageService;
    private final SimpMessagingTemplate messagingTemplate;

    public MessageController(MessageService messageService, SimpMessagingTemplate simpMessagingTemplate) {
        this.messageService = messageService;
        this.messagingTemplate = simpMessagingTemplate;
    }

    @GetMapping("message/between/{userId1}/{userId2}")
    public List<EnhancedMessageDTO> getMessagesInConversationsBetweenUsers(@PathVariable Integer userId1, @PathVariable Integer userId2) {
        List<MessageEntity> messages = messageService.getMessagesInConversationsBetweenUsers(userId1, userId2);
        return messages.stream().map(this::toEnhancedMessageDTO).collect(Collectors.toList());
    }
    @PostMapping("message/send")
    @Transactional
    public EnhancedMessageDTO sendMessageHttp(@RequestBody MessageDTO messageDTO) throws Exception {

        MessageEntity savedMessage = messageService.saveMessage(
                messageDTO.fromUserId(),
                messageDTO.toUserId(),
                messageDTO.messageText()
        );

        messagingTemplate.convertAndSend("/topic/chat/" + messageDTO.toUserId(), savedMessage);

        return toEnhancedMessageDTO(savedMessage);
    }

    private EnhancedMessageDTO toEnhancedMessageDTO(MessageEntity message) {

        Integer recipientId = findRecipientId(message);
        String senderFirstName = message.getUser().getFirstName();
        String senderLastName = message.getUser().getLastName();

        return new EnhancedMessageDTO(
                message.getIdMessage(),
                message.getUser().getIdUser(),
                senderFirstName,
                senderLastName,
                recipientId,
                message.getMessageText(),
                message.getCreatedAt()
        );
    }

    private Integer findRecipientId(MessageEntity message) {
        return message.getConversation().getParticipants().stream()
                .filter(participant -> !participant.getIdUser().equals(message.getUser().getIdUser()))
                .findFirst()
                .map(participant -> participant.getIdUser())
                .orElse(null);
    }

    @MessageMapping("/sendMessage")
    @SendTo("/topic/chat")
    @Transactional
    public MessageEntity sendMessage(@Payload MessageDTO messageDTO)throws Exception {
        // Save message to database
        MessageEntity savedMessage = messageService.saveMessage(
                messageDTO.fromUserId(),
                messageDTO.toUserId(),
                messageDTO.messageText()
        );

        messagingTemplate.convertAndSend("/topic/chat/" + messageDTO.toUserId(), savedMessage);

        return savedMessage;
    }

}