package com.arosaje.demo.controller;

import com.arosaje.demo.dto.AdviceDTO;
import com.arosaje.demo.input.AdviceInput;
import com.arosaje.demo.service.AdviceService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/advices")
public class AdviceController {

    private final AdviceService adviceService;

    public AdviceController(AdviceService adviceService) {
        this.adviceService = adviceService;
    }

    @PostMapping
    public ResponseEntity<AdviceDTO> createAdvice(@RequestBody AdviceInput adviceInput) {
        AdviceDTO adviceDTO = adviceService.createAdvice(adviceInput);
        return new ResponseEntity<>(adviceDTO, HttpStatus.CREATED);
    }

    @Operation(summary = "Get example by ID", description = "Retrieve an example by its ID", tags = { "example" })
    @GetMapping("/session/{idSession}/user/{idUser}")
    public ResponseEntity<List<AdviceDTO>> getAdvicesBySessionAndUser(
            @PathVariable int idSession,
            @PathVariable int idUser
    ) {
        List<AdviceDTO> advices = adviceService.getAdvicesBySessionAndUser(idSession, idUser);
        return ResponseEntity.ok(advices);
    }

    @GetMapping("/user/{idUser}")
    public ResponseEntity<List<AdviceDTO>> getAdvicesByUser(@PathVariable int idUser) {
        List<AdviceDTO> advices = adviceService.getAdvicesByUserId(idUser);
        return ResponseEntity.ok(advices);
    }
}
