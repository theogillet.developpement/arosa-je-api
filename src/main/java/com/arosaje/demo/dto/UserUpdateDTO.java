package com.arosaje.demo.dto;

import jakarta.annotation.Nullable;

public record UserUpdateDTO(
        @Nullable String firstName,
        @Nullable String lastName,
        @Nullable String email,
        @Nullable String address,
        @Nullable String phoneNumber,
        byte[] imageUser,

        int note
) {
}
