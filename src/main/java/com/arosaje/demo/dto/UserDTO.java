package com.arosaje.demo.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public record UserDTO(
        @NotBlank String firstName,
        @NotBlank String lastName,
        @NotBlank String email,
        @NotBlank String address,
        @NotBlank String phoneNumber,
        @NotBlank String password,
        byte[] imageUser,
        @NotNull Type_Enum userType,
        int note,
        @NotNull Double latitude,
        @NotNull Double longitude
) {
}
