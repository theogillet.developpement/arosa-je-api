package com.arosaje.demo.dto;


import com.arosaje.demo.entity.SessionEntity;

import java.sql.Timestamp;

public record PhotoDTO(
        Timestamp dateTaken,
        byte[] image,

        SessionEntity session
) {
}

