package com.arosaje.demo.dto;

public record LoginDTO( String email,
                        String password) {
}
