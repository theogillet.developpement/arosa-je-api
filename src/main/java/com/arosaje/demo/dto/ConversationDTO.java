package com.arosaje.demo.dto;

import java.util.List;

public record ConversationDTO(
        Long idConversation,
        String conversationName,
        List<ParticipantDTO> participants
) { }