package com.arosaje.demo.dto;


public record PlantDTO(
        String namePlant,
        String species,
        String location,
        int wateringFrequency,
        byte[] imagePlant,
        UserDTO userDTO
) {}
