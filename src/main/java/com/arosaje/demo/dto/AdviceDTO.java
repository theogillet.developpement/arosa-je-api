package com.arosaje.demo.dto;

public record AdviceDTO(
        String advice,
        SessionDTO sessionDTO,
        UserDTO userDTO
) {
}