package com.arosaje.demo.dto;

import java.sql.Timestamp;
import java.util.List;

public record SessionDTO(
        Timestamp startDate,
        Timestamp endDate,
        SessionStatus status,
        float latitude,
        float longitude,
        UserDTO userDTO,
        List<PlantDTO> plantDTO
) {
}
