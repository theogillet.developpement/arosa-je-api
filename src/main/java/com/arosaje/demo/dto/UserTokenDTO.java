package com.arosaje.demo.dto;

import com.arosaje.demo.entity.UserEntity;

public class UserTokenDTO {
    private UserEntity user;
    private String token;

    public UserTokenDTO(UserEntity user, String token) {
        this.user = user;
        this.token = token;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
