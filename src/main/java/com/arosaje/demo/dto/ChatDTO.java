package com.arosaje.demo.dto;

public record ChatDTO(
        String conversationName
) {
}
