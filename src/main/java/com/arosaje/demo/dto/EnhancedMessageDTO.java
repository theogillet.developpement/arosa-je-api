package com.arosaje.demo.dto;

import java.time.LocalDateTime;

public record EnhancedMessageDTO(
        Long idMessage,
        Integer fromUserId,
        String fromUserFirstName,
        String fromUserLastName,
        Integer toUserId,
        String messageText,
        LocalDateTime createdAt
) { }
