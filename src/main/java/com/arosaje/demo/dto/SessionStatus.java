package com.arosaje.demo.dto;

public enum SessionStatus {
    ACTIVE,
    INACTIVE,
    COMPLETED,
    REQUEST,
    INPROGRESS
}