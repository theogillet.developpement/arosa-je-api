package com.arosaje.demo.dto;

import java.time.LocalDateTime;

public record MessageDTO(
        Long idMessage,
        Integer fromUserId,
        Integer toUserId,
        Long converstionId,
        String messageText,
        LocalDateTime createdAt
) { }
