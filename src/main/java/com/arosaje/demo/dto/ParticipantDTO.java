package com.arosaje.demo.dto;

public record ParticipantDTO(Integer idUser,
                             String firstName,
                             String lastName) {

}
