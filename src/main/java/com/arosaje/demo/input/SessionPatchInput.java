package com.arosaje.demo.input;

public record SessionPatchInput(
        Integer sessionId,
        Integer userId
) {}
