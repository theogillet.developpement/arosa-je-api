package com.arosaje.demo.input;

import com.arosaje.demo.dto.SessionStatus;

import java.sql.Timestamp;
import java.util.List;

public record SessionInput(
        Timestamp startDate,
        Timestamp endDate,
        SessionStatus status,
        float latitude,
        float longitude,
        int idUser,
        int idUserLookingAfter,
        List<Integer> plantIds
) {
}
