package com.arosaje.demo.input;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import org.springframework.validation.annotation.Validated;

@Validated
public record PlantInput(@NotBlank String name,
                         @NotBlank String species,

                         @NotBlank String location,

                         @Min(0) int wateringFrequency,

                         @NotBlank byte[] image,

                          int idUser
) {
}
