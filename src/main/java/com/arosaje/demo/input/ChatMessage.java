package com.arosaje.demo.input;

public class ChatMessage {
    private Integer fromUserId;
    private Long toConversationId;
    private String content;

    public Integer getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(Integer fromUserId) {
        this.fromUserId = fromUserId;
    }

    public Long getToConversationId() {
        return toConversationId;
    }

    public void setToConversationId(Long toConversationId) {
        this.toConversationId = toConversationId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
