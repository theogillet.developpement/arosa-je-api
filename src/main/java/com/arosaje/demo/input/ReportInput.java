package com.arosaje.demo.input;

public record ReportInput(int idUser, int idReportingUser, String reason, String details) {
}
