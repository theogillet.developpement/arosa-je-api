package com.arosaje.demo.input;

import java.sql.Timestamp;

public record PhotoInput(
        Timestamp dateTaken,
        byte[] image,
        int idSession
) {
}

