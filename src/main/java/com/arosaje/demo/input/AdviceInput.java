package com.arosaje.demo.input;

public record AdviceInput(
        String advice,
        int sessionId,
        int userId
) {

}