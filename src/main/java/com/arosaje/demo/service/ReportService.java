package com.arosaje.demo.service;

import com.arosaje.demo.entity.ReportEntity;
import com.arosaje.demo.entity.UserEntity;
import com.arosaje.demo.repository.ReportRepository;
import com.arosaje.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class ReportService {

    @Autowired
    private ReportRepository reportRepository;

    @Autowired
    private UserRepository userRepository;

    public ReportEntity createReport(UserEntity reportedUser, UserEntity reportingUser, String reason, String details) {
        ReportEntity report = new ReportEntity();
        report.setReportedUser(reportedUser);
        report.setReportingUser(reportingUser);
        report.setReason(reason);
        report.setDetails(details);
        report.setReportDate(LocalDateTime.now());
        reportRepository.save(report);

        long reportCount = reportRepository.countByReportedUser(reportedUser);

        if (reportCount >= 10) {
            userRepository.delete(reportedUser);
        }

        return report;
    }
}
