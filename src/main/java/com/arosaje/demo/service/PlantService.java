package com.arosaje.demo.service;

import com.arosaje.demo.dto.UserDTO;
import com.arosaje.demo.dto.PlantDTO;
import com.arosaje.demo.entity.PlantEntity;
import com.arosaje.demo.entity.SessionEntity;
import com.arosaje.demo.entity.UserEntity;
import com.arosaje.demo.input.PlantInput;
import com.arosaje.demo.input.SessionInput;
import com.arosaje.demo.repository.PlantRepository;
import com.arosaje.demo.repository.SessionRepository;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import jakarta.validation.Valid;
import org.apache.commons.codec.binary.Base64;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Service
public class PlantService {

    private final PlantRepository plantRepository;
    private final UserService userService;
    private final SessionRepository sessionRepository;

    public PlantService(PlantRepository plantRepository, UserService userService, SessionRepository sessionRepository) {
        this.plantRepository = plantRepository;
        this.userService = userService;
        this.sessionRepository = sessionRepository;
    }

    public PlantEntity getPlantById(Integer id) {
        return plantRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("La plante avec l'ID " + id + " n'a pas été trouvée"));
    }

    public List<PlantEntity> getPlantsByUserId(Integer userId) {
        return plantRepository.findByUser_IdUser(userId);

    }

    @Transactional
    public boolean deletePlant(Integer id) {
        Optional<PlantEntity> existingPlant = plantRepository.findById(id);
        if (existingPlant.isPresent()) {
            PlantEntity plantToDelete = existingPlant.get();
            List<SessionEntity> sessions = sessionRepository.findByPlantsContaining(plantToDelete);
            if (!sessions.isEmpty()) {
                sessions.forEach(session -> session.getPlants().remove(plantToDelete));
                sessionRepository.saveAll(sessions);
            }
            plantRepository.delete(plantToDelete);
            return true;
        }
        return false;
    }

    @Transactional
    public PlantEntity createPlant(@Valid PlantInput plantInput, MultipartFile image) throws IOException {
        UserEntity utilisateur = userService.getUserbyId(plantInput.idUser());

        byte[] imageBytes = image.getBytes();

        PlantEntity nouvellePlante = new PlantEntity(
                plantInput.name(),
                plantInput.location(),
                plantInput.wateringFrequency(),
                plantInput.species(),
                image.getBytes(),
                utilisateur
        );

        PlantEntity planteEnregistre = plantRepository.save(nouvellePlante);

        return planteEnregistre;
    }
}
