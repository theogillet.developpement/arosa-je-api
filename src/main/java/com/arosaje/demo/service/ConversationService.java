package com.arosaje.demo.service;

import com.arosaje.demo.entity.ConversationEntity;
import com.arosaje.demo.entity.UserEntity;
import com.arosaje.demo.repository.ConversationRepository;
import com.arosaje.demo.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ConversationService {

    private final ConversationRepository conversationRepository;
    private final UserRepository userRepository;

    public ConversationService(ConversationRepository conversationRepository, UserRepository userRepository) {
        this.conversationRepository = conversationRepository;
        this.userRepository = userRepository;
    }

    public ConversationEntity getOrCreateConversation(Integer senderId, Integer receiverId) {
        List<ConversationEntity> conversations = conversationRepository.findByParticipants(senderId, receiverId);
        if (!conversations.isEmpty()) {
            return conversations.get(0);
        }
        ConversationEntity conversation = new ConversationEntity("Conversation between " + senderId + " and " + receiverId);
        UserEntity sender = userRepository.findById(senderId).orElseThrow();
        UserEntity receiver = userRepository.findById(receiverId).orElseThrow();
        conversation.addParticipant(sender);
        conversation.addParticipant(receiver);
        return conversationRepository.save(conversation);
    }
    public List<ConversationEntity> getConversationsForUser(Integer userId) {
        return conversationRepository.findByParticipants_IdUser(userId);
    }
    public ConversationEntity createConversation(Integer userId1, Integer userId2, String conversationName) {
        UserEntity user1 = userRepository.findById(userId1).orElseThrow(() -> new RuntimeException("User not found"));
        UserEntity user2 = userRepository.findById(userId2).orElseThrow(() -> new RuntimeException("User not found"));

        ConversationEntity conversation = new ConversationEntity(conversationName);
        conversation.addParticipant(user1);
        conversation.addParticipant(user2);

        return conversationRepository.save(conversation);
    }
}
