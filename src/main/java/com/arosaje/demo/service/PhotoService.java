package com.arosaje.demo.service;

import com.arosaje.demo.dto.PhotoDTO;
import com.arosaje.demo.entity.PhotoEntity;
import com.arosaje.demo.entity.SessionEntity;
import com.arosaje.demo.input.PhotoInput;
import com.arosaje.demo.repository.PhotoRepository;
import com.arosaje.demo.repository.SessionRepository;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PhotoService {

    @Autowired
    private PhotoRepository photoRepository;

    @Autowired
    private SessionRepository sessionRepository;

    @Transactional
    public PhotoDTO createPhoto(PhotoInput photoInput) {

        SessionEntity sessionEntity = sessionRepository.findById(photoInput.idSession())
                .orElseThrow(() -> new EntityNotFoundException("Session not found with ID: " + photoInput.idSession()));

        PhotoEntity photoEntity = convertToEntity(photoInput, sessionEntity);

        PhotoEntity savedPhotoEntity = photoRepository.save(photoEntity);

        return convertToDTO(savedPhotoEntity);
    }

    private PhotoEntity convertToEntity(PhotoInput photoInput, SessionEntity sessionEntity) {
        PhotoEntity photoEntity = new PhotoEntity();
        photoEntity.setDateTaken(photoInput.dateTaken());
        photoEntity.setImage(photoInput.image());
        photoEntity.setSession(sessionEntity); // Set the associated session
        return photoEntity;
    }

    private PhotoDTO convertToDTO(PhotoEntity photoEntity) {
        PhotoDTO photoDTO = new PhotoDTO(photoEntity.getDateTaken(), photoEntity.getImage(), photoEntity.getSession());
        return photoDTO;
    }
}