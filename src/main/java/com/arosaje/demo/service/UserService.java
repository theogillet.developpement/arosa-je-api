package com.arosaje.demo.service;

import com.arosaje.demo.dto.LoginDTO;
import com.arosaje.demo.dto.Type_Enum;
import com.arosaje.demo.dto.UserDTO;
import com.arosaje.demo.dto.UserUpdateDTO;
import com.arosaje.demo.entity.UserEntity;
import com.arosaje.demo.repository.UserRepository;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import jakarta.validation.Valid;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.util.List;
import java.util.Optional;

@Service
@Validated
public class UserService {

    private final UserRepository userRepository;


    private final BCryptPasswordEncoder passwordEncoder;

    public UserService(UserRepository userRepository, BCryptPasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public List<UserEntity> getAllUser() {
        return userRepository.findAll();
    }

    public void createUser(@Valid UserDTO userDTO) {
        if (userDTO.firstName() == null || userDTO.lastName() == null || userDTO.email() == null || userDTO.address() == null || userDTO.phoneNumber() == null || userDTO.password() == null || userDTO.latitude() == null || userDTO.longitude() == null) {
            throw new NullPointerException("All required fields must be not null.");
        }
        UserEntity user = setUser(userDTO);
        Optional<UserEntity> userByEmail = userRepository.findByEmail(user.getEmail());
        if (userByEmail.isPresent()) {
            throw new EntityExistsException("A user with this email already exists.");
        }
        userRepository.save(user);
    }

    public Boolean updateUser(int id, @Valid UserUpdateDTO userUpdateDTO) {
        if (userUpdateDTO == null) {
            throw new NullPointerException("No data provided.");
        }

        return userRepository.findById(id)
                .map(user -> {
                    if (userUpdateDTO.firstName() != null) {
                        user.setFirstName(userUpdateDTO.firstName());
                    }
                    if (userUpdateDTO.lastName() != null) {
                        user.setLastName(userUpdateDTO.lastName());
                    }
                    if (userUpdateDTO.email() != null) {
                        user.setEmail(userUpdateDTO.email());
                    }
                    if (userUpdateDTO.address() != null) {
                        user.setAddress(userUpdateDTO.address());
                    }
                    if (userUpdateDTO.phoneNumber() != null) {
                        user.setPhoneNumber(userUpdateDTO.phoneNumber());
                    }
                    if (userUpdateDTO.imageUser() != null) {
                        user.setImageUser(userUpdateDTO.imageUser());
                    }
                    if (userUpdateDTO.note() != -1) {
                        user.setNote(userUpdateDTO.note());
                    }

                    userRepository.save(user);
                    return true;
                })
                .orElse(false);
    }


    public UserEntity getUserByemailandPassword(LoginDTO loginDTO) {
        return userRepository.findByEmail(loginDTO.email())
                .filter(user -> passwordEncoder.matches(loginDTO.password(), user.getPassword()))
                .orElse(null);
    }

    public UserEntity getUserbyId(int idUser) {
        Optional<UserEntity> optionalUser = userRepository.findById(idUser);
        return optionalUser.orElseThrow(() -> new EntityNotFoundException("User not found with id : " + idUser));
    }

    public List<UserEntity> getBotanistUsersSortedByNote() {
        return userRepository.findByUserTypeOrderByNoteDesc(Type_Enum.BOTANIST);
    }

    private UserEntity setUser(UserDTO userDTO) {
        String motDePasseHash = passwordEncoder.encode(userDTO.password());
        UserEntity user = new UserEntity(
                userDTO.firstName(),
                userDTO.lastName(),
                userDTO.email(),
                userDTO.address(),
                userDTO.phoneNumber(),
                motDePasseHash,
                userDTO.note(),
                null,
                userDTO.userType(),
                userDTO.latitude(),
                userDTO.longitude()
        );
        return user;
    }

}
