package com.arosaje.demo.service;


import com.arosaje.demo.entity.ConversationEntity;
import com.arosaje.demo.entity.MessageEntity;
import com.arosaje.demo.entity.UserEntity;
import com.arosaje.demo.repository.ConversationRepository;
import com.arosaje.demo.repository.MessageRepository;
import com.arosaje.demo.repository.UserRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;


@Service
public class MessageService {

    private final UserRepository userRepository;
    private final ConversationService conversationService;
    private final MessageRepository messageRepository;

    @Autowired
    public MessageService(UserRepository userRepository, ConversationRepository conversationRepository, ConversationService conversationService, MessageRepository messageRepository) {
        this.userRepository = userRepository;
        this.conversationService = conversationService;
        this.messageRepository = messageRepository;
    }

    @Transactional
    public MessageEntity saveMessage(Integer fromUserId,Integer toUserId, String content) {
        UserEntity user = userRepository.findById(fromUserId).orElseThrow();
        ConversationEntity conversation = conversationService.getOrCreateConversation(fromUserId,toUserId);
        conversation.getParticipants().size();

        MessageEntity message = new MessageEntity();
        message.setUser(user);
        message.setConversation(conversation);
        message.setMessageText(content);
        message.setCreatedAt(LocalDateTime.now());



        return messageRepository.save(message);
    }

    public List<MessageEntity> getMessagesInConversationsBetweenUsers(Integer userId1, Integer userId2) {
        return messageRepository.findMessagesInConversationsBetweenUsers(userId1, userId2);
    }

}
