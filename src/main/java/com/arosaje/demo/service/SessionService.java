package com.arosaje.demo.service;

import com.arosaje.demo.dto.PlantDTO;
import com.arosaje.demo.dto.SessionDTO;
import com.arosaje.demo.dto.SessionStatus;
import com.arosaje.demo.dto.UserDTO;
import com.arosaje.demo.entity.PlantEntity;
import com.arosaje.demo.entity.SessionEntity;
import com.arosaje.demo.entity.UserEntity;
import com.arosaje.demo.input.SessionInput;
import com.arosaje.demo.repository.PlantRepository;
import com.arosaje.demo.repository.SessionRepository;
import com.arosaje.demo.repository.UserRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SessionService {

    private final SessionRepository sessionRepository;
    private final UserRepository userRepository;
    private final PlantRepository plantRepository;

    @Autowired
    public SessionService(SessionRepository sessionRepository, UserRepository userRepository, PlantRepository plantRepository) {
        this.sessionRepository = sessionRepository;
        this.userRepository = userRepository;
        this.plantRepository = plantRepository;
    }

    public List<SessionEntity> getSessionsByUser(Integer userId, SessionStatus status) {
        return sessionRepository.findByUser_IdUserAndStatus(userId, status);
    }

    public List<SessionEntity> getSessionsByUserWithoutCompleted(Integer userId) {
        return sessionRepository.findByUser_IdUserAndStatusNot(userId, SessionStatus.COMPLETED);
    }

    public List<SessionEntity> getSessionsByUserLookingAfterWithoutCompleted(Integer userIdLookingAfter) {
        return sessionRepository.findByUserLookingAfter_IdUserAndStatusNot(userIdLookingAfter, SessionStatus.COMPLETED);
    }

    public List<SessionEntity> getSessionsWithStatusNotUser(Integer userId) {
        return sessionRepository.findByUser_IdUserNotAndStatus(userId, SessionStatus.ACTIVE);
    }

    public List<SessionEntity> getActiveSessionsByDateRangeAndNotUser(Timestamp startDate, Timestamp endDate, Integer userId) {
        return sessionRepository.findByStatusAndStartDateGreaterThanEqualAndEndDateLessThanEqualAndUser_IdUserNot(
            SessionStatus.ACTIVE, startDate, endDate, userId
        );
    }
    
    public SessionEntity removeUserLookingAfterAndSetActive(Integer sessionId) {
        SessionEntity session = sessionRepository.findById(sessionId).orElseThrow(() -> new EntityNotFoundException("Session with ID " + sessionId + " not found"));
    
        session.setUserLookingAfter(null);
    
        session.setStatus(SessionStatus.ACTIVE);
    
        return sessionRepository.save(session);
    }

    public SessionEntity setSessionInProgress(Integer sessionId) {
        SessionEntity session = sessionRepository.findById(sessionId)
            .orElseThrow(() -> new EntityNotFoundException("Session not found"));

        session.setStatus(SessionStatus.INPROGRESS);
        return sessionRepository.save(session);
    }

    public SessionEntity createSession(SessionInput sessionInput) {
        List<PlantEntity> plantEntities = sessionInput.plantIds().stream()
                .map(plantId -> plantRepository.findById(plantId)
                        .orElseThrow(() -> new EntityNotFoundException("Plant with ID " + plantId + " not found")))
                .collect(Collectors.toList());

        SessionEntity sessionEntity = convertToEntity(sessionInput);

        sessionEntity.setPlants(plantEntities);

        SessionEntity savedSession = sessionRepository.save(sessionEntity);

        return savedSession;
    }


    public SessionEntity assignUserToSession(Integer sessionId, Integer userId) {
        SessionEntity session = getSessionById(sessionId);
        UserEntity user = userRepository.findById(userId)
                .orElseThrow(() -> new EntityNotFoundException("User with ID " + userId + " not found"));
        session.setStatus(SessionStatus.REQUEST);
        session.setUserLookingAfter(user);
        return sessionRepository.save(session);
    }

    private SessionEntity convertToEntity(SessionInput sessionInput) {
        SessionEntity sessionEntity = new SessionEntity();
        sessionEntity.setStartDate(sessionInput.startDate());
        sessionEntity.setLatitude(sessionInput.latitude());
        sessionEntity.setLongitude(sessionInput.longitude());
        sessionEntity.setEndDate(sessionInput.endDate());
        sessionEntity.setStatus(sessionInput.status());
        sessionEntity.setUser(userRepository.findById(sessionInput.idUser()).get());
        return sessionEntity;
    }

    private SessionDTO convertToDTO(SessionEntity sessionEntity) {
        List<PlantDTO> plantsDTO = sessionEntity.getPlants().stream()
                .map(this::mapPlantEntityToDTO)
                .collect(Collectors.toList());

        return new SessionDTO(sessionEntity.getStartDate(),
                sessionEntity.getEndDate(),
                sessionEntity.getStatus(),
                sessionEntity.getLatitude(),
                sessionEntity.getLongitude(),
                mapUserEntityToDTO(sessionEntity.getUser()),
                plantsDTO);
    }

    private PlantDTO mapPlantEntityToDTO(PlantEntity plantEntity) {
        return new PlantDTO(
                plantEntity.getNamePlant(),
                plantEntity.getSpecies(),
                plantEntity.getLocation(),
                plantEntity.getWateringFrequency(),
                plantEntity.getImagePlant(),
                mapUserEntityToDTO(plantEntity.getUser())
        );
    }

    private UserDTO mapUserEntityToDTO(UserEntity userEntity) {
        return new UserDTO(
                userEntity.getLastName(),
                userEntity.getFirstName(),
                userEntity.getEmail(),
                userEntity.getAddress(),
                userEntity.getPhoneNumber(),
                userEntity.getPassword(),
                userEntity.getImageUser(),
                userEntity.getUserType(),
                userEntity.getNote(),
                userEntity.getLatitude(),
                userEntity.getLongitude()
        );
    }

    public SessionEntity getSessionById(Integer sessionId) {
        return sessionRepository.findById(sessionId)
                .orElseThrow(() -> new EntityNotFoundException("Session with ID " + sessionId + " not found"));
    }
}
