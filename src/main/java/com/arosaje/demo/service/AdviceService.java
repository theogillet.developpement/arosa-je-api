package com.arosaje.demo.service;

import com.arosaje.demo.dto.AdviceDTO;
import com.arosaje.demo.dto.PlantDTO;
import com.arosaje.demo.entity.AdviceEntity;
import com.arosaje.demo.entity.PlantEntity;
import com.arosaje.demo.entity.SessionEntity;
import com.arosaje.demo.entity.UserEntity;
import com.arosaje.demo.input.AdviceInput;
import com.arosaje.demo.dto.UserDTO;
import com.arosaje.demo.dto.SessionDTO;
import com.arosaje.demo.repository.AdviceRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AdviceService {

    private final AdviceRepository adviceRepository;
    private final UserService userService;
    private final SessionService sessionService;

    public AdviceService(AdviceRepository adviceRepository,
                         UserService userService,
                         SessionService sessionService) {
        this.adviceRepository = adviceRepository;
        this.userService = userService;
        this.sessionService = sessionService;
    }

    public AdviceDTO createAdvice(AdviceInput adviceInput) {
        validateUser(adviceInput.userId());

        UserEntity userEntity = userService.getUserbyId(adviceInput.userId());
        SessionEntity sessionEntity = sessionService.getSessionById(adviceInput.sessionId());

        AdviceEntity adviceEntity = new AdviceEntity(adviceInput.advice(), userEntity, sessionEntity);
        AdviceEntity savedAdvice = adviceRepository.save(adviceEntity);

        return mapAdviceEntityToDTO(savedAdvice);
    }
    public List<AdviceDTO> getAdvicesByUserId(int userId) {
        List<AdviceEntity> advices = adviceRepository.findByUser_IdUser(userId);
        return advices.stream()
                .map(this::mapAdviceEntityToDTO)
                .collect(Collectors.toList());
    }

    private void validateUser(int userId) {
        userService.getUserbyId(userId);
    }

    private AdviceDTO mapAdviceEntityToDTO(AdviceEntity adviceEntity) {
        UserDTO userDTO = mapUserEntityToDTO(adviceEntity.getUser());
        SessionDTO sessionDTO = mapSessionEntityToDTO(adviceEntity.getSession());

        return new AdviceDTO(
                adviceEntity.getAdviceText(),
                sessionDTO,
                userDTO
        );
    }

    private UserDTO mapUserEntityToDTO(UserEntity userEntity) {
        return new UserDTO(
                userEntity.getLastName(),
                userEntity.getFirstName(),
                userEntity.getEmail(),
                userEntity.getAddress(),
                userEntity.getPhoneNumber(),
                userEntity.getPassword(),
                userEntity.getImageUser(),
                userEntity.getUserType(),
                userEntity.getNote(),
                userEntity.getLatitude(),
                userEntity.getLongitude()
        );
    }

    public List<AdviceDTO> getAdvicesBySessionAndUser(int idSession, int idUser) {
        List<AdviceEntity> advices = adviceRepository.findBySessionIdSessionAndUser_IdUser(idSession, idUser);
        return advices.stream()
                .map(this::mapAdviceEntityToDTO)
                .collect(Collectors.toList());
    }

    private SessionDTO mapSessionEntityToDTO(SessionEntity sessionEntity) {
        UserDTO userDTO = mapUserEntityToDTO(sessionEntity.getUser());

        List<PlantDTO> plantsDTO = sessionEntity.getPlants().stream()
                .map(this::mapPlantEntityToDTO)
                .collect(Collectors.toList());

        return new SessionDTO(
                sessionEntity.getStartDate(),
                sessionEntity.getEndDate(),
                sessionEntity.getStatus(),
                sessionEntity.getLatitude(),
                sessionEntity.getLongitude(),
                userDTO,
                plantsDTO
        );
    }

    private PlantDTO mapPlantEntityToDTO(PlantEntity plantEntity) {
        return new PlantDTO(
                plantEntity.getNamePlant(),
                plantEntity.getSpecies(),
                plantEntity.getLocation(),
                plantEntity.getWateringFrequency(),
                plantEntity.getImagePlant(),
                mapUserEntityToDTO(plantEntity.getUser())
        );
    }
}
