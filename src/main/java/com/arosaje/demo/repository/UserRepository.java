package com.arosaje.demo.repository;

import com.arosaje.demo.dto.Type_Enum;
import com.arosaje.demo.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Integer> {
    Optional<UserEntity> findByEmail(String email);

    List<UserEntity> findByUserType(Type_Enum userType);

    List<UserEntity> findByUserTypeOrderByNoteDesc(Type_Enum userType);

}
