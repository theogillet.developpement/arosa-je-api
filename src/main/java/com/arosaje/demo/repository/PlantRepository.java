package com.arosaje.demo.repository;

import com.arosaje.demo.entity.PlantEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlantRepository extends JpaRepository<PlantEntity, Integer> {

    List<PlantEntity> findByUser_IdUser(Integer idUser);

}
