package com.arosaje.demo.repository;

import com.arosaje.demo.dto.SessionStatus;
import com.arosaje.demo.entity.PlantEntity;
import com.arosaje.demo.entity.SessionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface SessionRepository extends JpaRepository<SessionEntity, Integer> {
    List<SessionEntity> findByUser_IdUser(Integer userId);

    List<SessionEntity> findByUser_IdUserAndStatus(Integer idUser, SessionStatus status);

    List<SessionEntity> findByUser_IdUserAndStatusNot(Integer idUser, SessionStatus status);

    List<SessionEntity> findByUserLookingAfter_IdUserAndStatusNot(Integer idUserLookingAfter, SessionStatus status);

    List<SessionEntity> findByUser_IdUserNotAndStatus(Integer idUser, SessionStatus status);
    
    List<SessionEntity> findByPlantsContaining(PlantEntity plant);

    List<SessionEntity> findByStatusAndStartDateGreaterThanEqualAndEndDateLessThanEqualAndUser_IdUserNot(
        SessionStatus status, Timestamp startDate, Timestamp endDate, Integer userId
    );
}