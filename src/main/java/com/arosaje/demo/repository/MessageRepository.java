package com.arosaje.demo.repository;

import com.arosaje.demo.entity.MessageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<MessageEntity, Long> {
    List<MessageEntity> findByConversation_IdConversation(Long conversationId);

    @Query("SELECT m FROM MessageEntity m WHERE m.conversation.idConversation IN " +
            "(SELECT c.idConversation FROM ConversationEntity c JOIN c.participants p1 JOIN c.participants p2 " +
            "WHERE p1.idUser = :userId1 AND p2.idUser = :userId2)")
    List<MessageEntity> findMessagesInConversationsBetweenUsers(@Param("userId1") Integer userId1, @Param("userId2") Integer userId2);
}
