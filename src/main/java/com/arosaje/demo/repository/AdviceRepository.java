package com.arosaje.demo.repository;

import com.arosaje.demo.entity.AdviceEntity;
import com.arosaje.demo.entity.SessionEntity;
import com.arosaje.demo.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AdviceRepository extends JpaRepository<AdviceEntity, Integer> {
    List<AdviceEntity> findBySessionIdSessionAndUser_IdUser(int idSession, int idUser);

    List<AdviceEntity> findBySessionAndUser(SessionEntity session, UserEntity user);
    List<AdviceEntity> findByUser_IdUser(int userId);

}