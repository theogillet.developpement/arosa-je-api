package com.arosaje.demo.repository;

import com.arosaje.demo.entity.ConversationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConversationRepository extends JpaRepository<ConversationEntity, Long> {
    @Query("SELECT c FROM ConversationEntity c JOIN c.participants p1 JOIN c.participants p2 WHERE p1.idUser = :senderId AND p2.idUser = :receiverId")
    List<ConversationEntity> findByParticipants(Integer senderId, Integer receiverId);
    List<ConversationEntity> findByParticipants_IdUser(Integer userId);
}