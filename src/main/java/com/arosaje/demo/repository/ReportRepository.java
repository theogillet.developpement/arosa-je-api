package com.arosaje.demo.repository;

import com.arosaje.demo.entity.ReportEntity;
import com.arosaje.demo.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReportRepository extends JpaRepository<ReportEntity, Long> {

    long countByReportedUser(UserEntity user);
}
