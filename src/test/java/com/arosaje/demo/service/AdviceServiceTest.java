package com.arosaje.demo.service;

import com.arosaje.demo.dto.AdviceDTO;
import com.arosaje.demo.dto.Type_Enum;
import com.arosaje.demo.entity.AdviceEntity;
import com.arosaje.demo.entity.SessionEntity;
import com.arosaje.demo.entity.UserEntity;
import com.arosaje.demo.input.AdviceInput;
import com.arosaje.demo.repository.AdviceRepository;
import com.arosaje.demo.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class AdviceServiceTest {

    @Mock
    private AdviceRepository adviceRepository;

    @Mock
    private UserService userService;

    @Mock
    private SessionService sessionService;

    @InjectMocks
    private AdviceService adviceService;

    @Mock
    private UserRepository userRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }


    @Test
    void getAdvicesByUserId_shouldReturnAdvices_whenValidUserId() {
        // Arrange
        int userId = 1;
        List<AdviceEntity> adviceEntities = new ArrayList<>();
        when(adviceRepository.findByUser_IdUser(userId)).thenReturn(adviceEntities);

        // Act
        List<AdviceDTO> advices = adviceService.getAdvicesByUserId(userId);

        // Assert
        assertEquals(adviceEntities.size(), advices.size());
    }

}
