package com.arosaje.demo.service;

import com.arosaje.demo.dto.PhotoDTO;
import com.arosaje.demo.entity.PhotoEntity;
import com.arosaje.demo.entity.SessionEntity;
import com.arosaje.demo.input.PhotoInput;
import com.arosaje.demo.repository.PhotoRepository;
import com.arosaje.demo.repository.SessionRepository;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class PhotoServiceTest {

    @Mock
    private PhotoRepository photoRepository;

    @Mock
    private SessionRepository sessionRepository;

    @InjectMocks
    private PhotoService photoService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void createPhoto_shouldCreatePhoto_whenSessionExists() {
        // Arrange
        int sessionId = 1;
        Timestamp dateTaken = Timestamp.valueOf(LocalDateTime.now());
        byte[] image = new byte[]{0x0, 0x1, 0x2};
        PhotoInput photoInput = new PhotoInput(dateTaken, image, sessionId);

        SessionEntity sessionEntity = new SessionEntity();
        sessionEntity.setIdSession(1);
        when(sessionRepository.findById(sessionId)).thenReturn(Optional.of(sessionEntity));

        PhotoEntity photoEntity = new PhotoEntity(dateTaken,image,sessionEntity);
        when(photoRepository.save(any())).thenReturn(photoEntity);


        PhotoDTO createdPhotoDTO = photoService.createPhoto(photoInput);

        assertNotNull(createdPhotoDTO);
        assertEquals(dateTaken, createdPhotoDTO.dateTaken());
        assertArrayEquals(image, createdPhotoDTO.image());
        assertEquals(sessionEntity, createdPhotoDTO.session());
        verify(sessionRepository, times(1)).findById(sessionId);
        verify(photoRepository, times(1)).save(any(PhotoEntity.class));
    }

    @Test
    void createPhoto_shouldThrowException_whenSessionDoesNotExist() {
        // Arrange
        int sessionId = 1;
        Timestamp dateTaken = Timestamp.valueOf(LocalDateTime.now());
        byte[] image = new byte[]{0x0, 0x1, 0x2}; // Example image bytes
        PhotoInput photoInput = new PhotoInput(dateTaken, image, sessionId);

        when(sessionRepository.findById(sessionId)).thenReturn(Optional.empty());

        // Act and Assert
        assertThrows(EntityNotFoundException.class, () -> photoService.createPhoto(photoInput));
        verify(sessionRepository, times(1)).findById(sessionId);
        verify(photoRepository, never()).save(any(PhotoEntity.class));
    }
}