package com.arosaje.demo.service;

import com.arosaje.demo.dto.Type_Enum;
import com.arosaje.demo.dto.UserDTO;
import com.arosaje.demo.entity.PlantEntity;
import com.arosaje.demo.entity.UserEntity;
import com.arosaje.demo.input.PlantInput;
import com.arosaje.demo.repository.PlantRepository;
import com.arosaje.demo.repository.SessionRepository;
import com.arosaje.demo.repository.UserRepository;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class PlantServiceTest {

    @Mock
    private PlantRepository plantRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserService userService;

    @Mock
    private SessionRepository sessionRepository;

    @InjectMocks
    private PlantService plantService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getPlantById_shouldReturnPlant_whenPlantExists() {
        // Arrange
        PlantEntity plantEntity = new PlantEntity();
        int plantId = 1;
        when(plantRepository.findById(plantId)).thenReturn(Optional.of(plantEntity));

        // Act
        PlantEntity result = plantService.getPlantById(plantId);

        // Assert
        assertNotNull(result);
        assertEquals(plantEntity, result);
    }

    @Test
    void getPlantById_shouldThrowException_whenPlantDoesNotExist() {
        // Arrange
        int plantId = 1;
        when(plantRepository.findById(plantId)).thenReturn(Optional.empty());

        // Act and Assert
        assertThrows(EntityNotFoundException.class, () -> plantService.getPlantById(plantId));
    }

    @Test
    void getPlantsByUserId_shouldReturnListOfPlants() {
        // Arrange
        List<PlantEntity> plants = new ArrayList<>();
        plants.add(new PlantEntity());
        plants.add(new PlantEntity());
        int userId = 1;
        when(plantRepository.findByUser_IdUser(userId)).thenReturn(plants);

        // Act
        List<PlantEntity> result = plantService.getPlantsByUserId(userId);

        // Assert
        assertEquals(plants.size(), result.size());
    }
    @Test
    void createPlant_shouldSavePlant_whenUserExists() throws IOException {
        PlantInput plantInput = new PlantInput("Plant Name", "Species", "Location", 3, new byte[0], 1);
        UserEntity userEntity = new UserEntity("John", "Doe", "john@example.com", "Address", "123456789", "password", 0,null, Type_Enum.USER, 44.801567, -0.606603);
        when(userService.getUserbyId(plantInput.idUser())).thenReturn(userEntity);

        MockMultipartFile image = new MockMultipartFile("image", new byte[0]);

        when(plantRepository.save(any())).thenAnswer(invocation -> {
            PlantEntity plantEntity = invocation.getArgument(0);
            plantEntity.setIdPlant(1);
            return plantEntity;
        });
        PlantEntity plantId = plantService.createPlant(plantInput, image);
        assertEquals(1, plantId.getIdPlant());
        verify(plantRepository, times(1)).save(any(PlantEntity.class));
    }
}
