package com.arosaje.demo.service;

import com.arosaje.demo.dto.LoginDTO;
import com.arosaje.demo.dto.UserDTO;
import com.arosaje.demo.dto.UserUpdateDTO;
import com.arosaje.demo.entity.UserEntity;
import com.arosaje.demo.repository.UserRepository;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.arosaje.demo.dto.Type_Enum.USER;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserService userService;

    @Mock
    private BCryptPasswordEncoder passwordEncoder;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }


    @Test
    void getAllUser_shouldReturnListOfUsers() {
        List<UserEntity> users = Arrays.asList(new UserEntity(), new UserEntity());
        when(userRepository.findAll()).thenReturn(users);

        List<UserEntity> result = userService.getAllUser();

        assertEquals(2, result.size());
    }

    @Test
    void updateUser_shouldReturnTrue_whenUserExists() {
        int userId = 1;
        UserUpdateDTO userDTO = new UserUpdateDTO("John", "Doe", "john.doe@example.com", "Address", "123456789",null, 0);
        UserEntity userEntity = new UserEntity("John", "Doe", "john.doe@example.com", "Address", "123456789", "password", 0, null,null,44.801567, -0.606603);
        when(userRepository.findById(userId)).thenReturn(Optional.of(userEntity));

        boolean result = userService.updateUser(userId, userDTO);

        assertTrue(result);
        verify(userRepository, times(1)).save(userEntity);
    }

    @Test
    void getUserByemailandPassword_shouldReturnUser_whenCredentialsMatch() {
        LoginDTO loginDTO = new LoginDTO("john.doe@example.com", "password");
        UserEntity userEntity = new UserEntity("John", "Doe", "john.doe@example.com", "Address", "123456789", "password", 0, null,null,44.801567, -0.606603);
        when(userRepository.findByEmail(loginDTO.email())).thenReturn(Optional.of(userEntity));
        when(passwordEncoder.matches(loginDTO.password(), userEntity.getPassword())).thenReturn(true);

        UserEntity result = userService.getUserByemailandPassword(loginDTO);

        assertNotNull(result);
        assertEquals(userEntity, result);
    }

    @Test
    void getUserbyId_shouldReturnUser_whenUserExists() {
        int userId = 1;
        UserEntity userEntity = new UserEntity("John", "Doe", "john.doe@example.com", "Address", "123456789", "password", 0, null,null,44.801567, -0.606603);
        when(userRepository.findById(userId)).thenReturn(Optional.of(userEntity));

        UserEntity result = userService.getUserbyId(userId);

        assertNotNull(result);
        assertEquals(userEntity, result);
    }

    @Test
    void getUserbyId_shouldThrowException_whenUserDoesNotExist() {
        int userId = 1;
        when(userRepository.findById(userId)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> userService.getUserbyId(userId));
    }

    @Test
    void createUser_shouldThrowException_whenUserDTOIsNull() {
        assertThrows(NullPointerException.class, () -> userService.createUser(null));
    }

    @Test
    void createUser_shouldThrowException_whenFirstNameIsNull() {
        UserDTO userDTO = new UserDTO(null, "Doe", "john.doe@example.com", "Address", "123456789", "password", null, USER , 0, 44.801567, -0.606603);
        assertThrows(NullPointerException.class, () -> userService.createUser(userDTO));
    }

    @Test
    void createUser_shouldThrowException_whenLastNameIsNull() {
        UserDTO userDTO = new UserDTO("John", null, "john.doe@example.com", "Address", "123456789", "password", null, USER, 0, 44.801567, -0.606603);
        assertThrows(NullPointerException.class, () -> userService.createUser(userDTO));
    }


    @Test
    void createUser_shouldSaveUser_whenNoUserExistsWithSameEmail() {
        // Arrange
        UserDTO userDTO = new UserDTO("John", "Doe", "john.doe@example.com", "Address", "123456789", "password", null, USER, 0, 44.801567, -0.606603);
        when(userRepository.findByEmail(userDTO.email())).thenReturn(Optional.empty());

        userService.createUser(userDTO);

        verify(userRepository, times(1)).save(any(UserEntity.class));
    }

}
