setup:
	docker compose down -v
	docker compose up --build -d
	./mvnw flyway:migrate -Dflyway.configFiles=./myFlywayConfig.conf
